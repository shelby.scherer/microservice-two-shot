import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList';
import HatsForm from './HatsForm';
import ShoesForm from './ShoesForm';
import ShoeList from './ShoesList';

function App() {

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index path="/" element={<MainPage />} />
            <Route path="hats" element={<HatsList/> }/>
            <Route path="hats/new" element={<HatsForm />} />
    
          <Route path="shoes" element={<ShoeList />} />
          <Route path="shoes/new" element={<ShoesForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
