import React from "react"  


class HatsForm extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            fabric: "",
            style_name: "",
            hat_color: "",
            haturl: "",
            location: "",
            locations: [],
        }
        
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeFabric = this.handleChangeFabric.bind(this);
        this.handleChangeStyle = this.handleChangeName.bind(this);
        this.handleChangeColor = this.handleChangeColor.bind(this);
        this.handleChangePicture = this.handleChangePicture.bind(this);
        this.handleChangeLocation = this.handleChangeLocation.bind(this);

    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({ locations: data.locations });
        }
    }


    handleSubmit = async (event) => {
        event.preventDefault();
        const data = {...this.state};
        data.hat_url =data.haturl;
        delete data.haturl;
        delete data.locations;
     
        const hatsUrl = "http://localhost:8090/api/hats/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(hatsUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            this.setState = {
                fabric: "",
                style_name: "",
                hat_color: "",
                haturl: "",
                location: "",
            };
            window.location.href="/hats"

        }
        

    }
    handleChangeName(event) {
        const value = event.target.value;
        this.setState({ style_name: value });
    }

    handleChangeFabric(event) {
        const value = event.target.value;
        this.setState({ fabric: value });
    }

    handleChangeColor(event) {
        const value = event.target.value;
        this.setState({ color: value });
    }

    handleChangePicture(event) {
        const value = event.target.value;
        this.setState({ haturl: value });
    }

    handleChangeLocation(event) {
        const value = event.target.value;
        this.setState({ location: value });
    }

    
    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <p>hellow</p>
                <h1>Add New Hat</h1>
                <form onSubmit={this.handleSubmit} id="create-hat-form">
                <div className="form-floating mb-3">
                    <input onChange={this.handleChangeName} value={this.state.style_name} placeholder="Style name" required type="text" name="style_name" id="style_name" className="form-control"/>
                    <label htmlFor="style_name">Style Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeFabric} value={this.state.fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control"/>
                    <label htmlFor="fabric">Fabric</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeColor} value={this.state.color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                    <label htmlFor="color">Color</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangePicture} value={this.state.hat_url} placeholder="Picture" required type="url" name="hat_url" id="hat_url" className="form-control"/>
                    <label htmlFor="hat_url">Picture</label>
                  </div>
                  <div className="mb-3">
                    <select onChange={this.handleChangeLocation} value={this.state.location} required id="location" name="location" className="form-select">
                      <option value="">Choose Location</option>
                    {this.state.locations.map(location => {
                        return (
                            <option key={location.id} value={location.href}>
                                {location.closet_name}
                            </option>
                        )
                    })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Add New Hat</button>
                </form>
              </div>
            </div>
          </div>
        )
    }
}

export default HatsForm;
