import React from 'react';

class ShoesForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            manufacturer: '',
            color: '',
            pictureurl: '',
            bins: [],
        };


        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeManufacturer = this.handleChangeManufacturer.bind(this);
        this.handleChangeColor = this.handleChangeColor.bind(this);
        this.handleChangePicture = this.handleChangePicture.bind(this);
        this.handleChangeBin = this.handleChangeBin.bind(this);
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/bins/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({ bins: data.bins });
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        data.picture_url = data.pictureurl;
        delete data.pictureurl;
        delete data.bins;
        console.log(data);

        const binUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(binUrl, fetchConfig);
        if (response.ok) {
            const newShoes = await response.json();
            console.log(newShoes);
            this.setState({
                name: '',
                manufacturer: '',
                color: '',
                pictureurl: '',
                bin: '',
            });
        }
        window.open("/shoes");
    }

    handleChangeName(event) {
        const value = event.target.value;
        this.setState({ name: value });
    }

    handleChangeManufacturer(event) {
        const value = event.target.value;
        this.setState({ manufacturer: value });
    }

    handleChangeColor(event) {
        const value = event.target.value;
        this.setState({ color: value });
    }

    handleChangePicture(event) {
        const value = event.target.value;
        this.setState({ pictureurl: value });
    }

    handleChangeBin(event) {
        const value = event.target.value;
        this.setState({ bin: value });
    }


    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add a pair of shoes</h1>
                        <form onSubmit={this.handleSubmit} id="create-shoes-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChangeName} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChangeManufacturer} value={this.state.manufacturer} placeholder="Manufacture" name="manufacture" id="manufacture" className="form-control" />
                                <label htmlFor="manufacturer">Manufacturer</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChangeColor} value={this.state.color} placeholder="Color" name="color" id="color" className="form-control" />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChangePicture} value={this.state.pictureurl} className="form-control" id="pictureurl" name="picture_URL" />
                                <label htmlFor="pictureurl">Picture URL</label>
                            </div>

                            <div className="mb-3">
                                <select onChange={this.handleChangeBin} value={this.state.bin} required name="bin" id="bin" className="form-select">
                                    <option value="">Choose a bin</option>
                                    {this.state.bins.map(bin => {
                                        return (
                                            <option key={bin.id} value={bin.href}>{bin.id}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default ShoesForm;
