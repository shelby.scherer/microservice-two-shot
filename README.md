# Wardrobify

Team:
* Xue Yu - Shoes
* Israel Navarrete - Hats

## Design

○  Diagram of the Architecture
    [a relative link](diagram.png)

○  How to run the application

    ■ docker volume create pgdata

    ■ docker-compose build

    ■ docker-compose up

## Shoes microservice

○  The urls and ports

    ■ The main page: http://localhost:3000/
        The main page shows the shoes and has a button to create a pair of new shoes.

    ■ The shoes page: http://localhost:3000/shoes/
        It has a table to show detailed information about all shoes, with a delete button at the end of each row.
        Clicking the delete button will remove the corresponding pair of shoes. It will disappear immediately on the page.

    ■ The form to create new shoes page: http://localhost:3000/shoes/new/
        User can create a pair of new shoes here. When click "Create" button, the new information
        will be saved and show up on both main and shoes pages.

○ CRUD routes

    ■ GET the list of shoes: http://localhost:8080/api/shoes/

    ■ POST a new pair of shoes: http://localhost:8080/api/shoes/
        example:
        {
        "manufacturer": "Nike",
        "name":"airforce1",
        "color":"blue",
        "picture_url":"https://encrypted-tbn2.gstatic.com/shopping?q=tbn:ANd9GcTWDs9BDGSgAnvWhMekLycgP9SSCJUxssti5e8JrHXYj-XuUesPgXZceA3TQfF4012d6_acrvt3Emg&usqp=CAc",
        "bin":"/api/locations/1/"
        }

    ■ GET the detail of a pair of shoes, e.g. shoes with href "/api/shoes/7/": http://localhost:8080/api/shoes/7/

    ■ PUT part of the information of a pair of shoes: http://localhost:8080/api/shoes/7/
        example:
        {
        "manufacturer": "New Balance",
        "bin":"/api/locations/2/"
        }
    ■ DELETE a pair of shoes: http://localhost:8080/api/shoes/7/


○ The Bin Value Object:

    Bin information is in the wardrobe microservice. If we want to use it in the shoes microservice, we need to get a copy of all the bin information. So that the BinVO is created to polling the information periodically.


## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
